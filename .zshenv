#!/usr/bin/env zsh


### Variáveis ambientais

## Path
export PATH=$PATH:"$HOME/.config/commands"

### Adiciona as coisas do Haskell ao PATH
[ -f "/home/vitor/.ghcup/env" ] && source "/home/vitor/.ghcup/env" # ghcup-env

### Adiciona as coisas do pip ao PATH
export PATH=$PATH:"$HOME/.local/bin"

### Julia
case ":$PATH:" in
    *:/home/vitor/.juliaup/bin:*)
        ;;

    *)
        export PATH=/home/vitor/.juliaup/bin${PATH:+:${PATH}}
        ;;
esac


## Terminal
export TERM='xterm-kitty'

## Muda o local dos arquivos de configuração 
export ZDOTDIR=$HOME/.zsh

## Editor 
export EDITOR='nvim'
export VISUAL='nvim'

## Histórico de comandos 
export HISTFILE=$ZDOTDIR/histfile
export HISTSIZE=1000
export SAVEHIST=1000


## Paginas dos manuais
## Usa o bat para colorir as páginas dos manuais
#export MANPAGER="sh -c 'col -bx | bat -l man -p'"


## GPG
## O gpg diz que eu deveria adicionar essas linhas.
## ver man gpg-agent. Por que? Não sei
## GPG_TTY=$(tty)
## export GPG_TTY


## Utilidades
export DOTSDIR="$HOME/.dotfiles"
export CONFIGDIR="$HOME/.config"
export SCRIPTSDIR="$HOME/.config/scripts"
export ROFITHEMESDIR="$HOME/.config/rofi/temas"
export TEXTEMPLATEDIR="$HOME/.config/tex/modelos"
export NOTESDIR="$HOME/NT-Notas"
