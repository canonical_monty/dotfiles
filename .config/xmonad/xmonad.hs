{-# LANGUAGE FlexibleContexts #-}
-- Configurações xmonad --
--


-- Importações
import XMonad 

-- Layouts
--
-- Para espaço entre as janelas
import XMonad.Layout.Spacing

-- Layouts propriamente
import XMonad.Layout.Grid
import XMonad.Layout.Dishes
import XMonad.Layout.TwoPane 
-- import XMonad.Layout.TwoPanePersistent
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.LayoutHints
import XMonad.Layout.LayoutModifier
-- import XMonad.Layout.Groups
-- import XMonad.Layout.Groups.Examples
-- import XMonad.Layout.Groups.Wmii
-- 
-- import XMonad.Layout.SubLayouts
-- import XMonad.Layout.BoringWindows
-- import XMonad.Layout.WindowNavigation



-- Ações
-- 
-- Para ter submapas nas configurações do teclado.
import XMonad.Actions.Submap

-- Para rotacionar apenas as janelas servas. Resolve o problema de TwoPanes
-- Por algum motivo não funciona com TwoPanesPersistent mas funciona com 
-- TwoPanes e dá o comportamento esperado.
import XMonad.Actions.RotSlaves
-- Ver também RotateSome

-- TODO
-- Correlato ao anterior. Me parece útil mas ainda não olhei.
-- import XMonad.Actions.CycleWindows



-- Ganchos
--
-- Adequação ao padrão EWMH
import XMonad.Hooks.EwmhDesktops

-- Ordem de inserção de janelas
import XMonad.Hooks.InsertPosition

-- Para respeitar a Polybar
import XMonad.Hooks.ManageDocks

-- Para controlar o XMonad externamente
import XMonad.Hooks.ServerMode

-- Para lidar com o foco de janelas flutuantes
import XMonad.Hooks.RefocusLast

import XMonad.Hooks.ManageHelpers

-- Utilidades
--
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig

-- Funções para manipular janelas e clientes.
import qualified XMonad.StackSet as W


-- Teclas extras --- Fn.
import Graphics.X11.ExtraTypes.XF86

-- Mapas --- dicionários.
import qualified Data.Map as M
import Data.Maybe (isNothing, fromJust)




-- Configuração dos arranjos
--
-- Para renomear os arranjos
renameLayout :: String -> l a -> ModifiedLayout Rename l a
renameLayout n = renamed [Replace n]

-- Configurações comuns
--
-- Por algum motivo se eu colocar `hints`, `gaps` e `borders` como está no where 
-- dá erro. AAAAAAAAAAAAAAAAAAAAAAAAAAAA! Daí tive que separar em funções assim.
-- Aparenemente se eu colocar isso aqui no início do arquivo funciona:
--      {-# LANGUAGE FlexibleContexts #-}
--      {-# LANGUAGE NoMonomorphismRestriction #-}
-- Não faço a menor ideia do porquê.
hints :: (LayoutClass l a) => l a -> ModifiedLayout LayoutHints l a
hints = layoutHintsWithPlacement (0.5, 0.5)

gaps :: (LayoutClass l a) => l a -> ModifiedLayout Spacing l a
gaps = spacingRaw False edgeBorder True windowBorder True
    where
        edgeBorder = (Border 60 8 8 8)
        windowBorder = (Border 8 8 8 8)

borders :: (LayoutClass l a) => l a -> ModifiedLayout (ConfigurableBorder Ambiguity) l a
borders = lessBorders OnlyScreenFloat


-- Agrupa as configurações comuns acima.
-- Sem usar `FlexibleContexts` que é uma extensão do GHC eu não consigo inserir
-- `hint` porque ele fica falando que não consegue deduzir `LayoutModifier LayoutHints a`.
tileConfig :: (Eq a, LayoutModifier LayoutHints a, LayoutClass l a) => l a -> ModifiedLayout (ConfigurableBorder Ambiguity) (ModifiedLayout Spacing (ModifiedLayout AvoidStruts (ModifiedLayout LayoutHints l))) a
tileConfig = borders . gaps . avoidStruts . hints



myLayout = refocusLastLayoutHook $
    renameLayout "tall"         (tileConfig . hints $ tall             ) ||| 
    renameLayout "wide"         (tileConfig . hints $ Mirror $ tall    ) |||
    renameLayout "monocle"      (tileConfig . hints $ Full             ) |||
    renameLayout "work"         (tileConfig . hints $ twoPane          ) |||
    renameLayout "fullscreen"   (layoutHints $ noBorders Full   )
    where
        tall = Tall nmaster delta ratio
        twoPane = TwoPane delta ratio

        -- Parâmetros
        nmaster = 1
        delta = 3/100
        ratio = 1/2

        -- -- Configurações de borda, espaçamento e afins
        -- config = borders . gaps . avoidStruts . hints

        -- -- Elimina as bordas das janelas quando elas estão flutuantes e
        -- -- preenchendo toda a tela. Combina com `ewmhFullscreen`.
        -- borders = lessBorders OnlyScreenFloat

        -- -- Adiciona espaço entre as janelas e em relação aos limites da tela
        -- gaps = spacingRaw False edgeBorder True windowBorder True
        -- edgeBorder = (Border 60 8 8 8)
        -- windowBorder = (Border 8 8 8 8)

        -- -- Faz com que as janelas respeitem as dicas de tamanho. Adicionalmente
        -- -- as posiciona no centro.
        -- hints = layoutHintsWithPlacement (0.5, 0.5)



-- Configurações gerais --
-- 

-- Aplicativos
--
-- Terminal preferido.
myTerminal :: String
myTerminal = "kitty"

-- Aplicativos gerais
web_browser = spawn "firefox"

-- TODO: adequar os nomes
-- Menus do Rofi
rofi_launcher = spawn "rofi -show run"
rofi_powermenu = spawn "$SCRIPTSDIR/power-script"
rofi_screenshot = spawn "$SCRIPTSDIR/screenshot-script"
rofi_library = spawn "$SCRIPTSDIR/pdfs"
rofi_books = spawn "$SCRIPTSDIR/library-launcher '/home/vitor/LV-Livros' 0"
rofi_articles = spawn "$SCRIPTSDIR/library-launcher '/home/vitor/AT-Artigos' 0"
rofi_rpg = spawn "$SCRIPTSDIR/library-launcher '/home/vitor/LR-Livros de RPG' 0"



-- Outras configurações
--
-- Se o foco deve seguir o mouse.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

-- Se o clique do mouse seleciona o foco.
myClickJustFocuses :: Bool
myClickJustFocuses = True


-- Bordas
--
-- Cor de bordas
myNormalBorderColor :: String
myNormalBorderColor = "#369282"

myFocusedBorderColor :: String
myFocusedBorderColor = "#F7AB00"

-- Grossura da borda
myBorderWidth :: Dimension
myBorderWidth = 2


-- Áreas de trabalho
myWorkspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
myWorkspacesKeys = [xK_1, xK_2, xK_3, xK_4, xK_5, xK_6, xK_7, xK_8, xK_9, xK_0]



-- Teclado e mouse --
-- 

-- Modkey
myModMask = mod4Mask
altMask = mod1Mask

-- Funções auxiliares
-- Achei na internet. Acho que com isso eu eventualmente consigo fazer
-- algo para impedir que o rotSlave{Up, Down} afete janelas flutuantes.
-- Em particular para poder usar o Peek para gravar. Nada urgente.
-- skipFloating :: (Eq a, Ord a) => W.StackSet i l a s sd -> (W.StackSet i l a s sd -> W.StackSet i l a s sd) -> W.StackSet i l a s sd
-- skipFloating stacks f
--     | isNothing curr = stacks -- short circuit if there is no currently focused window
--     | otherwise = skipFloatingR stacks curr f
--   where curr = W.peek stacks
-- 
-- 
-- skipFloatingR :: (Eq a, Ord a) => W.StackSet i l a s sd -> (Maybe a) -> (W.StackSet i l a s sd -> W.StackSet i l a s sd) -> W.StackSet i l a s sd
-- skipFloatingR stacks startWindow f
--     | isNothing nextWindow = stacks -- next window is nothing return current stack set
--     | nextWindow == startWindow = newStacks -- if next window is the starting window then return the new stack set
--     | M.notMember (fromJust nextWindow) (W.floating stacks) = newStacks -- if next window is not a floating window return the new stack set
--     | otherwise = skipFloatingR newStacks startWindow f -- the next window is a floating window so keep recursing (looking)
--   where newStacks = f stacks
--         nextWindow = W.peek newStacks
-- 

-- Por algum motivo o arranjo `fullscreen` impede o `JumpToLayout`. Daí
-- é necessário primeiro voltar para o arranjo padrão e depois pular para
-- o próximo arranjo.
goToLayout l c = do
    setLayout $ XMonad.layoutHook c 
    sendMessage $ JumpToLayout l



-- Teclado
-- Aparentemente o parâmetro `keys` recebe uma função com essa assinatura.
-- Não entendo muito bem a assinatura mas (X ()) eu acredito seja o um comando
-- de modo que a função retorna um mapa --- dicionário --- de combinações de 
-- teclas e o comando correspondente.
myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())

-- O comando @ é uma expressão idiomática para desestruturação. Significa enquadre
-- `conf` **como** o que segue. Ou seja, `conf` é todo o registro Layout passado mas
-- o valor do campo `XMonad.modMask` é capturado e chamado de `modMask` para ser
-- usado no corpo da função.
myKeys conf@(XConfig  {XMonad.modMask = super}) = M.fromList $
    
    -- O operador .|. é um ou bit a bit.
    -- Funcionalidades gerais
    --
    -- Abre o terminal
    [   ((super .|. shiftMask,      xK_Return),     spawn $ XMonad.terminal conf) 

    -- Firefox
    ,   ((super .|. shiftMask,      xK_f),          web_browser)

    -- Rofi
    ,   ((super,                    xK_space),      rofi_launcher)
    ,   ((super .|. shiftMask,      xK_p),          rofi_powermenu)
    ,   ((super .|. shiftMask,      xK_Print),      rofi_screenshot)

    -- Menu para abrir livros e artigos. Usa um submapa.
    ,   ((super,                    xK_a),          submap . M.fromList $
        [
            ((0, xK_m), rofi_library)
        ,   ((0, xK_l), rofi_books)
        ,   ((0, xK_a), rofi_articles)
        ,   ((0, xK_r), rofi_rpg)
        ])


    -- Audio
    ,   ((0,                        xF86XK_AudioRaiseVolume),   spawn "volume --inc")
    ,   ((0,                        xF86XK_AudioLowerVolume),   spawn "volume --dec")



    -- Gerenciação de layouts
    --
    -- Move para o próximo layout
    ,   ((super,                    xK_e),          sendMessage NextLayout)

    -- Volta para o layout padrão
    ,   ((super,                    xK_r),          setLayout $ XMonad.layoutHook conf)

    -- Pula para layouts específicos.
    ,   ((super,                    xK_q),          submap . M.fromList $
        [
            ((0, xK_w),          goToLayout "work" conf)
        ,   ((0, xK_t),          goToLayout "tall" conf)
        ,   ((0, xK_m),          goToLayout "monocle" conf)
        ,   ((0, xK_f),          goToLayout "fullscreen" conf)
        ])


    -- Gerenciação de janelas
    --
    -- Fecha a janela focada
    ,   ((super .|. shiftMask,      xK_w),          kill)

    -- Manda janela flutuante de volta para o mosaico.
    ,   ((super,                    xK_t),          withFocused $ windows . W.sink)

    -- Move o foco para a janela mestra.
    ,   ((super,                    xK_m),          windows W.focusMaster)

    -- Troca janela focada com a janela mestra.
    ,   ((super,                    xK_s),          windows W.swapMaster)

    -- Move o foco para a próxima janela.
    ,   ((super,                    xK_Tab),        rotAllDown) -- windows W.focusDown)
    ,   ((super,                    xK_k),          windows W.focusDown)
    ,   ((super,                    xK_Right),      windows W.focusDown)

    -- Move o foco para a janela anterior.
    ,   ((super .|. shiftMask,      xK_Tab),        rotAllUp) -- windows W.focusUp)
    ,   ((super,                    xK_j),          windows W.focusUp)
    ,   ((super,                    xK_Left),       windows W.focusUp)

    -- Troca a janela focada com a próxima janela.
    ,   ((super .|. shiftMask,      xK_k),          windows W.swapDown)
    ,   ((super .|. shiftMask,      xK_Right),      windows W.swapDown)

    -- Troca a janela focada com a janela anterior.
    ,   ((super .|. shiftMask,      xK_h),          windows W.swapUp)
    ,   ((super .|. shiftMask,      xK_Left),       windows W.swapUp)

    -- Circula entre as janelas empilhadas no modo TwoPanePersistent #hack
    ,   ((super,                    xK_c),          rotSlavesUp)
    ,   ((super,                    xK_Up),         rotSlavesUp)

    -- Foca na janela anterior e a move para o topo da pilha.
    ,   ((super .|. shiftMask,      xK_c),          rotSlavesDown)
    ,   ((super .|. shiftMask,      xK_Down),       rotSlavesDown)


    -- Diminui a área mestra.
    ,   ((super,                    xK_h),          sendMessage Shrink)
    ,   ((super .|. controlMask,    xK_Left),       sendMessage Shrink)

    -- Aumenta a área mestra.
    ,   ((super,                    xK_l),          sendMessage Expand)
    ,   ((super .|. controlMask,    xK_Right),      sendMessage Expand)

    -- Aumenta o número de janelas na área mestra.
    ,   ((super,                    xK_comma),      sendMessage (IncMasterN 1))

    -- Diminui o número de janelas na área mestra.
    ,   ((super,                    xK_period),     sendMessage (IncMasterN (-1)))


    -- Reinicia o xmonad
    ,   ((super .|. altMask,        xK_r),          spawn "xmonad --recompile; xmonad --restart")
    ]

    ++

    -- Áreas de trabalho
    --
    -- mod-[1..9, 0] para troca para a área N
    -- mod-shift-[1..9, 0] move cliente para área N.
    --
    -- A sintaxe [ xx | xx <- xx ] indica compreensão de listas. A compreensão
    -- dupla --- xx <-- xx, yy <- yy --- combina os elementos de ambas as listas.
    [((super .|. m, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) myWorkspacesKeys
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
    ]



-- Gerenciamento de janela
--
-- Muda a ordem de inserção das janelas exceto para as 
-- que seriam flutuantes.
myManageHook = composeAll
    [   fmap not willFloat --> insertPosition Below Newer
    -- , isFullscreen --> liftX (sendMessage (JumpToLayout "tall")) >> doIgnore
    -- ,   getAtom 
    ]


-- Inicialização
--
myStartupHook :: X ()
myStartupHook = do
    spawn "zsh ~/.config/bin/startup"
-- myStartupHook = do
--         spawnOnce "feh --no-fehbg --bg-fill '/home/vitor/IM-Imagens/WP-Papeis de Parede/Yellow Grass - Dominik Mayer.jpg'"




main :: IO ()
main = xmonad . ewmhFullscreen . ewmh . docks $ def 
    {
        terminal            = myTerminal
    ,   normalBorderColor   = myNormalBorderColor
    ,   focusedBorderColor  = myFocusedBorderColor
    ,   borderWidth         = myBorderWidth

    ,   modMask             = myModMask
    ,   focusFollowsMouse   = myFocusFollowsMouse
    ,   clickJustFocuses    = myClickJustFocuses

    ,   workspaces          = myWorkspaces
    ,   keys                = myKeys

    ,   layoutHook          = myLayout 
    ,   startupHook         = myStartupHook
    ,   manageHook          = myManageHook
    ,   handleEventHook     = refocusLastWhen pred <> serverModeEventHook
    }   where
            pred = refocusingIsActive <||> isFloat

