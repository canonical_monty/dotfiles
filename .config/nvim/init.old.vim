"" Configurações do Neovim

"" As configurações usam os diferentes arquivos abaixo
"" para configurar aspectos diferentes.

source $HOME/.config/nvim/config/general.vim
source $HOME/.config/nvim/config/keys.vim
source $HOME/.config/nvim/config/theme.vim
source $HOME/.config/nvim/config/plug-init.vim
source $HOME/.config/nvim/config/plug-config.vim
