"" Recarrega as configurações do vim
nnoremap <leader>sv :source $MYVIMRC<CR>


"" Remove o destacamento de busca ao apertar esc
nnoremap <silent> <ESC> :noh<CR>

"" Ativar e desativar a verificação ortográfica
nnoremap <silent> <F11> :set spell!<CR>
inoremap <silent> <F11> <C-O>:set spell!<CR>

"" Movimentação em linhas emendadas
nnoremap <C-j> gj
nnoremap <C-k> gk
nnoremap <C-4> g$
nnoremap <C-0> g0

vnoremap <C-j> gj
vnoremap <C-k> gk
vnoremap <C-4> g$
vnoremap <C-0> g0


"" wiki.vim
nnoremap <silent> <s-CR> <plug>(wiki-link-follow-tab)


"" Telescope
nnoremap <silent> <leader>ff <cmd>Telescope find_files<cr>

"" LaTeX
"" Ver ftplugin/tex.vim
