"" Configurações dos plugins


"" Vimtex
let g:vimtex_view_method = 'zathura'
let g:vimtex_compiler_method = 'latexmk'

""" Define o motor padrão como xelatex
let g:vimtex_compiler_latexmk_engines = {
    \ '_'                   : '-xelatex',
    \}

""" Configurações do latexmk
let g:vimtex_compiler_latexmk = {
    \ 'out_dir'           : 'Build',
    \}

""" Desativa a exibição de avisos. Os erros podem ser vistos com VimtexErrors
let g:vimtex_quickfix_open_on_warning = 0

""" Fecha a janela de quickfix automaticamente ao pressionar algo.
let g:vimtex_quickfix_autoclose_after_keystrokes = 1




"" Ultisnips
let g:UltiSnipsSnippetDirectories = ["ultisnips"]
let g:UltiSnipsEditSplit = 'tabdo'



"" wiki.vim

"""let g:wiki_root = '~/WK-Wiki'

""" Função para encontar a raiz de uma wiki. Permite ter raizes
""" locais.
function! WikiRoot()
    let l:local = finddir('Wiki', ';./')
    return !empty(l:local) ? l:local : '~/WK-Wiki'
endfunction

let g:wiki_root = 'WikiRoot'


let g:wiki_filetypes = ['note']



"" Zen Mode
"" lua << EOF
""     require("zen-mode").setup {
""         window = {
""             width = 100,
"" 
""             options = {
""                 number = false,
""                 relativenumber = false,
""             },
""         },
""     }
"" EOF

"" Indent blanklines
"" Configurar os grupos de destacamento. A cor do `scope` deve ser mais
"" escura.
lua << EOF
    require("ibl").setup {
        indent = {
            highlight = { "Comment" },
        },
        scope = {
            show_start = false,
            show_end = false,
            highlight = { "Keyword" },
        },
    }
EOF


"" Treesitter
lua << EOF
    require("nvim-treesitter.configs").setup {
        highlight = {
            enable = true,
            disable = { "latex", "julia" },  -- Desabilitar para estas linguas
        },
        indent = {
            enable = true,
            disable = { "rust", "julia" },  -- Desabilitar para estas linguas
        },
    }
EOF
    


"" Telescope
lua << EOF
    require('telescope').setup{

        defaults = {
            mappings = {
                i = {
                    ["<CR>"] = "select_tab",
                    ["<ESC>"] = "close",
                },
            },
        },

    }
EOF


"" ZK
lua << EOF
    require("zk").setup({
        -- can be "telescope", "fzf" or "select" (`vim.ui.select`)
        -- it's recommended to use "telescope" or "fzf"
        picker = "fzf",

        lsp = {
            -- `config` is passed to `vim.lsp.start_client(config)`
            config = {
                cmd = { "zk", "lsp" },
                name = "zk",
                -- on_attach = ...
                -- etc, see `:h vim.lsp.start_client()`
            },

            -- automatically attach buffers in a zk notebook that match the given filetypes
            auto_attach = {
                enabled = true,
                filetypes = { "markdown" },
            },
        },
    })
EOF
