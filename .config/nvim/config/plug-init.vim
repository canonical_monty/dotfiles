"" Inicializa os plugins
call plug#begin()
" Use aspas simples ' '!


call plug#begin()

"" Vimtex
Plug 'lervag/vimtex'

"" Ultisnips
Plug 'SirVer/ultisnips'

"" Vimwiki
Plug 'lervag/wiki.vim'

"" TrueZen
Plug 'Pocco81/TrueZen.nvim'

"" Zen Mode
Plug 'folke/zen-mode.nvim'

"" Goyo
"" Plug 'junegunn/goyo.vim'


""Indent Blankline
Plug 'lukas-reineke/indent-blankline.nvim'

""Colorizer
Plug 'chrisbra/Colorizer'

"" Treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

"" Treesitter Playground
"" Usado para ver informações do treesitter no neovim, tipo
"" grupos de destacamento de sintaxe.
Plug 'nvim-treesitter/playground'

"" Telescope
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.4' }

"" Coc
"" Plug 'neoclide/coc.nvim', {'branch': 'release'}

"" Julia
Plug 'JuliaEditorSupport/julia-vim'

"" ZK
Plug 'mickael-menu/zk-nvim'

"" Just
Plug 'NoahTheDuke/vim-just'

"" Rescript
Plug 'rescript-lang/vim-rescript'




"" Inicializa os plugins
call plug#end()

