"" Configurações gerais do Neovim

filetype on				            " Detecção automático do tipo de arquivo.
syntax on				            " Destacamento de sintaxe.


set number				            " Mostra a numeração das linhas.
set relativenumber			        " Mostra a numeração relativa.


set inccommand=split		        " Mostra o resultado de um comando incrementalmente
					                " conforme ele é digitado. Split cria também uma 
					                " outra janela mostrando apenas o comando.

set shiftwidth=4			        " Número de espaços da identação.
set tabstop=4				        " Número de espaços do TAB.
set softtabstop=4			        " Define o número de espaços inseridos pelo TAB.
set expandtab				        " Transforma os TAB's em espaços.


set foldmethod=marker               " Método de dobragem de linhas
set foldmarker=\<\|\|,\|\|\>        " Define os marcadores para delimitar dobras

set mouse=a                         " Ativa o mouse
set spelllang=pt                    " Define o corretor ortográfico


"" Menu pop-up
""set pumwidth=100                    " Define a largura mínima do menu pop-up usado
                                    " para autocompleção.

set completeopt+=noinsert




" Função para achar o grupo de sintaxe sob o cursor. (Para usar digite :call SynGroup())
function! SynGroup()                                                            
    let l:s = synID(line('.'), col('.'), 1)                                       
    echo synIDattr(l:s, 'name') . ' -> ' . synIDattr(synIDtrans(l:s), 'name')
endfun


" Configurações do netrw
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3
let g:netrw_winsize = 25


"" Esquema de cores
"colorscheme great-sir
colorscheme yellow_grass


