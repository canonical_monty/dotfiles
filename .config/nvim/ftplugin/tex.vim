inoremap <buffer> <c-f> <esc>:call CreateTikzFigure()<cr>
nnoremap <buffer> <c-g> <esc>:call OpenTikzFigure()<cr>



"" Essa guarda é necessária porque CreateTikzFigure, ao chamar :tabnew
"" cria outro buffer do mesmo tipo --- arquivos .tikz tem filetype = tex.
"" Logo, o novo arquivo lê este arquivo e tenta definir novamente a função
"" CreateTikzFigure, mas como ela está em uso, isto resulta em um erro.
""
"" Outra solução seria colocar um arquivo em autoload. Ver:
"" https://stackoverflow.com/questions/22633115/why-do-i-get-e127-from-this-vimscript
if exists('*CreateTikzFigure')
    finish
endif

function CreateTikzFigure()

    let name = getline(".")
    let root = b:vimtex.root
    "" :exec '.!mktikzfigure "' . name . '" "' . root . '/Tikz/"'


    let figure_name = system('mktikzfigure prep "' . name . '"')
    :exec '.!mktikzfigure run "' . name . '" "' . root . '/Tikz/"'
    :exec ':tabnew ./Tikz/' . figure_name

endfunction


function OpenTikzFigure()

    let line = getline(".")
    let figure_name = system('mktikzfigure open "' . line . '"')
    if v:shell_error == 0
        :exec ':tabnew ./Tikz/' . figure_name
    else
        echoerr 'Algum erro ocorreu ao tentar abrir a figura.'
        if v:shell_error == 9
            echoerr 'Falha ao apreender o nome da figura ou figura inexistente.'
        endif
    endif

endfunction
