--
-- Configurações gerais
--

local g = vim.g             -- Variáveis globais
local opt = vim.opt         -- Opções


-- Lider
-- 
vim.g.mapleader = '\\'                                      -- Tecla lider


-- Gerais
--
opt.mouse = 'a'                                             -- Suporte do mouse
opt.completeopt = 'menuone,noinsert,noselect'               -- Autocompleção
opt.spelllang = 'pt'                                        -- Verificação ortográfica


-- UI
--
opt.number = true                                   -- Mostra número de linhas
opt.relativenumber = true                           -- Número de linha relativo
opt.inccommand = 'split'                            -- Mostra comandos incrementalmente
opt.foldmethod = 'marker'                           -- Método de dobra
opt.foldmarker = '<||,||>'                          -- Marcador de dobra   


-- Tabs e indentação
--
opt.shiftwidth = 4                                  -- Número de espaços na identação
opt.expandtab = true                                -- Transforma TAB em espaços
opt.tabstop = 4                                     -- Número de espaços no TAB
opt.smartindent = true                              -- Indenta automaticamente novas linhas


-- Netrw
g.netrw_liststyle = 3
g.netrw_browse_split = 3
g.netrw_winsize = 25

