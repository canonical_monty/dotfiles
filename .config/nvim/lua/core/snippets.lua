--
-- Snippets
--

local ls = require('luasnip')


-- Snippets para LaTeX
local tex = require('snippets.tex')
ls.add_snippets("tex", tex)
