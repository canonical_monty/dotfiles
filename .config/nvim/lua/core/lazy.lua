--
-- Gerenciamento de plugins
--


-- Inicializando lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)


-- Usa chamada protegida para evitar erros no primeiro uso
local status_ok, lazy = pcall(require, 'lazy')
if not status_ok then
    return
end


lazy.setup('plugins')
