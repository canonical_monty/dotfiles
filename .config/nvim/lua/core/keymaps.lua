--
-- Mapas de teclas
--

local function map(mode, lhs, rhs, opts)
    local options = { noremap = true, silent = true }
    if opts then
        options = vim.tbl_extend('force', options, opts)
    end
    -- vim.keymap.set(mode, lhs, rhs, options)
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end



-- Atalhos
--

-- Desabilita setinhas
map('', '<Up>', '<NOP>')
map('', '<Down>', '<NOP>')
map('', '<Left>', '<NOP>')
map('', '<Right>', '<NOP>')


-- Movimento entre janelas
map('n', '<C-h>', '<C-w>h')
map('n', '<C-j>', '<C-w>j')
map('n', '<C-k>', '<C-w>k')
map('n', '<C-l>', '<C-w>l')


-- Limpa destacamento de busca com <leader>-c
map('n', '<leader>c', ':nohls<CR>')

-- Recarrega as configurações do nvim
map('n', '<leader>r', ':so %<CR>')

-- Ativa e desativa verificação ortográfica
map('n', '<F11>', ':set spell!<CR>')

-- Salvamento rápido com <leader>-s
map('n', '<leader>s', ':w<CR>')

-- Atalho para ESC
map('i', 'kk', '<ESC>')


--
-- Plugins
--

-- Oil
--
-- Abre uma janela vertical com o Oil.
map('n', '-', ':25vs<CR>:Oil<CR>')


-- Luasnips
--
local ls = require('luasnip')

-- Usar '<Tab>' só para expandir os snippets é okay. 
-- Para se mover entre eles é horrível!
vim.keymap.set(
    'i', '<Tab>',
    function()
        if ls.expandable() then
            -- plugin/luasnip.lua no repositório tem a lista de todos
            -- os atalhos definidos com Plug.
            return '<Plug>luasnip-expand-snippet'
        else
            return '<Tab>'
        end
    end,
    { expr = true}
)

-- Para guardar a seleção visual. Ver luasnip:store_selection_keys
vim.keymap.set('v', '<Tab>', ls.select_keys)

-- Para movimentar entre os nodes de inserção.
-- Pula para o próximo ponto de inserção.
vim.keymap.set(
    'i', 'jk',
    function()
        if ls.jumpable(1) then
            ls.jump(1)
            -- return '<Plug>luasnip-jump-next'
        end
    end
)

-- Volta para o ponto de inserção anterior.
vim.keymap.set(
    'i', 'kj',
    function()
        if ls.jumpable(-1) then
            ls.jump(-1)
            -- return '<Plug>luasnip-jump-prev'
        end
    end
)




-- vim.keymap.set(
--     'i', '<A-e>', 
--     function()
--         if ls.expand_or_jumpable() then
--             ls.expand_or_jump()
--         end
--     end
-- )
-- 
-- 
-- -- Retorna para o ponto de inserção anterior.
-- vim.keymap.set(
--     'i', '<A-S-e>',
--     function()
--         ls.jump(-1)
--     end
-- )


