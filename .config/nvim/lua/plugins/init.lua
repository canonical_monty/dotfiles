local PluginList = {}


-- Temas
local Colorschemes = {
    {
        dir = vim.fn.stdpath('config') .. '/themes/yellow_grass',
        lazy = false,
        priority = 1000,
        config = function()
            require('yellow_grass').load()
        end,
    },
}


-- Suporte para linguages
local LangPlugins = {
    {
        'lervag/vimtex',
        config = function()
            require('plugins.config.vimtex')
        end,
    },

    {
        'JuliaEditorSupport/julia-vim',
    },

    {
        'NoahTheDuke/vim-just',
    },

    {
        'rescript-lang/vim-rescript',
    },

    -- Suporte para Go e em particular o tipo de arquivo `gohtmltmpl` que é útil
    -- para editar templates do Hugo.
    {
        'fatih/vim-go',
    },
}


-- Plugins
local Plugins = {
    -- Telescope
    {
        'nvim-telescope/telescope.nvim', tag = '0.1.4',
        dependencies = { 
            'nvim-lua/plenary.nvim' 
        },
    },


    -- FZF Lua
    {
        "ibhagwan/fzf-lua",
    },


    -- Zk
    {
        'mickael-menu/zk-nvim',
        config = function()
            require('plugins.config.zk')
        end,
    },


    -- Treesitter
    {
        'nvim-treesitter/nvim-treesitter',
        build = ":TSUpdate",
        config = function()
            require('plugins.config.treesitter')
        end,
    },

    {
        'nvim-treesitter/playground',
    },

    
    -- Indent Blankline
    {
        'lukas-reineke/indent-blankline.nvim',
        config = function()
            require('plugins.config.indent-blankline')
        end,
    },


    -- Colorizer
    {
        'chrisbra/Colorizer',
    },


    -- Zen Mode
    {
        'folke/zen-mode.nvim',
        config = function()
            require('plugins.config.zen-mode')
        end,
    },


    -- Oil
    {
        'stevearc/oil.nvim',
        config = function()
            require('plugins.config.oil')
        end,
        dependencies = { 'nvim-tree/nvim-web-devicons' },
    },


    -- LuaSnip
    {
        'L3MON4D3/LuaSnip',
	    version = "v2.1.0",
        config = function()
            require('plugins.config.luasnip')
        end,
    }


    -- -- Nvim-tree
    -- { 
    --     'kyazdani42/nvim-tree.lua', 
    --     lazy = false,
    -- },
}



table.insert(PluginList, Colorschemes)
table.insert(PluginList, LangPlugins)
table.insert(PluginList, Plugins)
return PluginList
