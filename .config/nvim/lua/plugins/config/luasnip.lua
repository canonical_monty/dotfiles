
require('luasnip').config.set_config({

    -- Faz com que nodes com rep, que repetem o texto, sejam
    -- atualizados conforme você escreve.
    update_events = 'TextChanged, TextChangedI',

    -- Eventos que fazem com que o luasnip saia de um snip.
    -- Colocar o cursor parado por um tempo ou mover ele 
    -- quando está fora da região do snip faz o luasnip entender
    -- o snippet como concluído.
    region_check_events = 'CursorHold, CursorMoved, InsertEnter',

})
