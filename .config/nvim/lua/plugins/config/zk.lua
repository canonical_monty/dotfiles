--
-- Zk
--

require("zk").setup {

    -- Usa o fzf para selecionar notas
    picker = 'fzf_lua',

    -- Servidor de linguagem
    lsp = {
        config = {
            cmd = { 'zk', 'lsp' },
            name = 'zk',
        },

        auto_attach = {
            enabled = true,
            filetypes = { 'markdown' },
        },
    },
}
