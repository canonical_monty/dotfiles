--
-- Treesitter
--


require('nvim-treesitter.configs').setup {
    highlight = {
        enable = true,
        disable = { 'latex', 'julia', 'rust', 'lua', 'python' },
    },

    indent =  {
        enable = false,
        -- disable = { 'rust', 'julia' },
    },

}
