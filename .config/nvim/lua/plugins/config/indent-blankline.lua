--
-- Indent-Blankline
--


require('ibl').setup {
    scope = {
        show_start = false,
        show_end = false,
        include = {
            node_type = {
                python = {
                    "if_statement",
                    "for_statement",
                    "while_statement",
                },
            },
        },
    },

    exclude = {
        filetypes = {
            'markdown',
        },
    },
}


