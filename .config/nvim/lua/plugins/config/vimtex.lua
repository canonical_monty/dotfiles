--
-- Vimtex
--

local g = vim.g


g.vimtex_view_method = 'zathura'                -- Usa o zatura para mostrar o arquivo.
g.vimtex_compiler_method = 'latexmk'            -- Usa o latexmk para compilar.

-- Configurações do latexmk
g.vimtex_compiler_latexmk_engines = {
    _ = '-xelatex',                             -- Usa o XeLaTeX por padrão.
}

g.vimtex_compiler_latexmk = {
    out_dir = 'build',                          -- Coloca os arquivos resultantes em build.
}



-- Janela de avisos
g.vimtex_quickfix_open_on_warning = 0               -- Desativa a janela de avisos para warnings.
g.vimtex_quickfix_autoclose_after_keystrokes = 1    -- Fecha a janela de avisos ao apertar 1 tecla.

