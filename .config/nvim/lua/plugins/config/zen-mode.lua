--
-- Zen Mode
--

require('zen-mode').setup {
    window = {
        backdrop = 1,
        width = 100,

        options = {
            number = false,
            relativenumber = false,
        },
    },
}
