local ls = require('luasnip')
local extras = require('luasnip.extras')
local fmta = require('luasnip.extras.fmt').fmta
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local d = ls.dynamic_node
local sn = ls.snippet_node
local rep = extras.rep



-- Função usada para substituir seleção visual
-- Fonte: https://www.ejmastnak.com/tutorials/vim-latex/luasnip/#writing
--
-- Quando eu aperto TAB --- ver atalho para 'select_key' --- no modo visual, 
-- o texto é deletado e armazenado em variáveis *SELECT* do snippet. 
-- Esta função, feita para ser usada com `dynamic_node`, obtem o texto 
-- armazenado em LS_SELECT_RAW e o coloca em um `insert_snippet` que
-- é então substituído no texto.
local get_visual = function(args, parent)
    if (#parent.snippet.env.LS_SELECT_RAW > 0) then
        return sn(nil, i(1, parent.snippet.env.LS_SELECT_RAW))
    else
        return sn(nil, i(1))
    end
end



local S = {

    -- Para '\begin' e '\end' para algum ambiente.
    ls.snippet(
        { trig = "\\beg" },
        fmta(
            [[
                \begin{<>}
                    <>
                \end{<>}
            ]],
            {
                i(1), i(2), rep(1)
            }
        )
    ),

    -- Para superíndices
    ls.snippet(
        { trig = "%*%*", trigEngine = "pattern", wordTrig = false },
        fmta(
            [[
                ^{<>}
            ]],
            {
                -- f( function(_, snip) return snip.captures[1] end ),
                i(1),
            }
        )
    ),

    -- Para subíndices
    ls.snippet(
        { trig = "__", trigEngine = "pattern", wordTrig = false },
        fmta(
            [[
                _{<>}
            ]],
            {
                -- f( function(_, snip) return snip.captures[1] end ),
                i(1),
            }
        )
    ),


    -- Negrito
    -- Insere o comando \textbf
    ls.snippet(
        { trig = "\\bf" },
        fmta(
            [[
                \textbf{<>}
            ]],
            {
                i(1)
            }
        )
    ),

    -- Substitui a seleção visual
    ls.snippet(
        { trig = "tbf" },
        fmta(
            [[
                \textbf{<>}
            ]],
            {
                d(1, get_visual),
            }
        )
    ),


    -- Itálico
    -- Insere o comando \textit
    ls.snippet(
        { trig = "\\it" },
        fmta(
            [[
                \textit{<>}
            ]],
            {
                i(1)
            }
        )
    ),

    -- Substitui a seleção visual
    ls.snippet(
        { trig = "tit" },
        fmta(
            [[
                \textit{<>}
            ]],
            {
                d(1, get_visual),
            }
        )
    ),


    -- Prepara a selação para ser posta dentro de um comando
    ls.snippet(
        { trig = "cc" },
        fmta(
            [[
                \<>{<>}
            ]],
            {
                i(1),
                d(2, get_visual),
            }
        )
    ),
}



return S
