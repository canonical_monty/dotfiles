--[[

Arquivo inicial do Neovim

--]]


-- Importa os módulos do Lua
require('core.options')             -- Opções gerais
require('core.lazy')                -- Gerenciador de plugins
require('core.keymaps')             -- Atalhos, incluindo plugins
require('core.snippets')

