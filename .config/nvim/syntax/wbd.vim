"" Arquivo de sintaxe vim
"" Para arquivos .wbd, meu formato pessoal para
"" minhas notas de criação de mundo e atividades
"" correlatas porque eu tenho tempo demais.
"" Mantenedor: Vitor Monteiro Pereira
"" Última revisão: 01/07/2022

if exists("b:current_syntax")
    finish
endif


"" Títulos
syn region H1  matchgroup=HeaderDelim  start="^\s*::"      end="$"  contains=HeaderLink
syn region H2  matchgroup=HeaderDelim  start="^\s*:::"     end="$"  contains=HeaderLink 
syn region H3  matchgroup=HeaderDelim  start="^\s*::::"    end="$"  contains=SubHeaderLink
syn region H4  matchgroup=HeaderDelim  start="^\s*:::::"   end="$"  contains=SubHeaderLink  


hi def link H1  Header
hi def link H2  Header
hi def link H3  SubHeader
hi def link H4  SubHeader

"" Links
syn region Bold     matchgroup=EmphSymbol    start="\*\*"    end="\*\*"  concealends
syn region Italic   matchgroup=EmphSymbol    start="__"      end="__"    concealends
syn region Emph     matchgroup=EmphSymbol    start="!!"      end="!!"    concealends

syn region Link             matchgroup=LinkDelimiter    start="\[\[" end="\]\]"  concealends
syn region HeaderLink       matchgroup=LinkDelimiter    start="\[\[" end="\]\]"  concealends contained 

syn region SubHeaderLink    matchgroup=LinkDelimiter    start="\[\[" end="\]\]"  concealends contained 

"" Cores (provisório)
hi HeaderDelim     cterm=bold      ctermfg=16      ctermbg=NONE
hi Header          cterm=bold      ctermfg=13      ctermbg=NONE
hi SubHeader       cterm=NONE      ctermfg=13      ctermbg=NONE
hi Link            cterm=underline ctermfg=16      ctermbg=NONE    guisp=#ED75A6
hi HeaderLink      cterm=bold,underline    ctermfg=13      ctermbg=NONE    guisp=#ED75A6
hi SubHeaderLink   cterm=underline         ctermfg=13      ctermbg=NONE    guisp=#ED75A6

hi Bold            cterm=bold      ctermfg=NONE    ctermbg=NONE
hi Italic          cterm=italic    ctermfg=NONE    ctermbg=NONE
hi Emph            cterm=NONE      ctermfg=13      ctermbg=NONE


syn match MdLink '\[\w\+\](\w\+)' contains=MdDescriptionReg,MdLinkReg
syn region MdDescriptionReg    matchgroup=LinkDelimiter start="\[" end="\]" concealends contained
syn region MdLinkReg           matchgroup=LinkDelimiter start="(" end=")"   concealends conceal contained

hi def link MdDescriptionReg Link
hi def link MdLinkReg Emph


