"" Arquivo de sintaxe vim
"" Para arquivos .rnt, meu formato pessoal para
"" notas de coisas de RPG.
"" Mantenedor: Vitor Monteiro Pereira
"" Última revisão: 31/07/2022

if exists("b:current_syntax")
    finish
endif


"" Títulos
syn region H1  matchgroup=HeaderDelim  start="^\s*::"      end="$"  contains=HeaderLink
syn region H2  matchgroup=HeaderDelim  start="^\s*:::"     end="$"  contains=HeaderLink 
syn region H3  matchgroup=HeaderDelim  start="^\s*::::"    end="$"  contains=SubHeaderLink
syn region H4  matchgroup=HeaderDelim  start="^\s*:::::"   end="$"  contains=SubHeaderLink  

hi def link H1  Header
hi def link H2  Header
hi def link H3  SubHeader
hi def link H4  SubHeader


syn region  Bold     matchgroup=EmphSymbol    start="\*\*"    end="\*\*"  concealends
syn region  Italic   matchgroup=EmphSymbol    start="__"      end="__"    concealends
syn region  Emph     matchgroup=EmphSymbol    start="!!"      end="!!"    concealends

syn match   Pointer     ">>\+"

syn keyword Maintainer  Mantenedor              contained
syn keyword Date        Data                    contained
syn region  Meta        matchgroup=MetaSymbol   start="-|"  end="|-"    concealends contains=Maintainer,Date


hi def link Maintainer Header
hi def link Date Header


"" Cores (provisório)
hi HeaderDelim      cterm=bold      ctermfg=16      ctermbg=NONE
hi Header           cterm=bold      ctermfg=13      ctermbg=NONE
hi SubHeader        cterm=NONE      ctermfg=13      ctermbg=NONE
hi Bold             cterm=bold      ctermfg=NONE    ctermbg=NONE
hi Italic           cterm=italic    ctermfg=NONE    ctermbg=NONE
hi Emph             cterm=NONE      ctermfg=13      ctermbg=NONE
hi Pointer          cterm=bold      ctermfg=16      ctermbg=NONE



