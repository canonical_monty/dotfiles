":hi @function cterm=bold ctermfg=NONE

"" :hi def link @function TexFunction
"" 
"" :hi def link @text.environment MyTest
"" 
"" :hi @namespace  cterm=bold  ctermfg=13  ctermbg=NONE
"" :hi @text.environment           cterm=bold      ctermfg=NONE    ctermbg=NONE
"" :hi @text.environment.name      cterm=bold      ctermfg=13      ctermbg=NONE


"" Configurações particulares baseados no vimtex
:hi texRefArg       cterm=bold      ctermbg=NONE    ctermfg=24
:hi texMathArg      cterm=NONE      ctermbg=NONE    ctermfg=24

:hi texMathSuperSub cterm=bold      ctermbg=NONE    ctermfg=16
:hi texMathDelim    cterm=bold      ctermbg=NONE    ctermfg=16
:hi texMathOper     cterm=bold      ctermbg=NONE    ctermfg=16
":hi Special     cterm=NONE      ctermfg=
"
"
" TODO: Usar a ideia de que coisas menos relevantes devem ficar mais apagadas
"
" Olha direitinho MatchParen
