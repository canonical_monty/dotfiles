local util = {}
local theme = require("great-sir-gui.theme")

-- Percorre a tabela de cores para os grupos de sintaxe e
-- atribui as cores e estilos. Parâmetros não especificados
-- recebem o valor NONE.
util.highlight = function(group, color)

    local style = color.style and "gui = " .. color.style or "gui = NONE"
    local fg = color.fg and "guifg = " .. color.fg or "guifg = NONE"
    local bg = color.bg and "guibg = " .. color.bg or "guibg = NONE"
    local sp = color.sp and "guisp = " .. color.sp or "guisp = NONE"

    local hl = "highlight " .. group .. " " .. style .. " " .. fg .. " " .. bg .. " " .. sp

    vim.cmd(hl)
    if color.link then
        vim.cmd("highlight! link " .. group .. " " .. color.link)
    end
end

-- Percorre o dicionário colorSet e destaca cada grupo do dicionário
-- segundo os valores da tabela usando highlight.
function util.loadColorSet(colorSet)
	for group, colors in pairs(colorSet) do
		util.highlight(group, colors)
	end
end


-- Carrega o tema
function util.load()

	-- Define o ambiente do tema
	vim.cmd("hi clear")
	if vim.fn.exists("syntax_on") then
		vim.cmd("syntax reset")
	end

	vim.o.background = "dark"
	vim.o.termguicolors = true
	vim.g.colors_name = "great-sir-gui"

	-- Importa as partes mais importantes do tema
	local editor = theme.loadEditor()
	local syntax = theme.loadSyntax()
    local tex = theme.loadTex()

	-- Carrega os destacamentos do editor
	util.loadColorSet(editor)

	-- Carrega os destacamentos de sintaxe
	util.loadColorSet(syntax)

    -- Carrega os destacamentos do vimtex
    util.loadColorSet(tex)
end

return util
