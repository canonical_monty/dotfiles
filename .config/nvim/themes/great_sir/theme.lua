local theme = {}
local pallete = require('great-sir-gui.pallete')

-- Estilos
local bold = "bold"
local italic = "italic"
local nocombine = "nocombine"


-- Tema dos grupos básicos
theme.loadSyntax = function()

    local syntax =
    {
        Statement = { fg = pallete.PR05, style = bold },
        Identifier = { fg = pallete.BG07 },
        PreProc = { style = bold },
        Constant = { fg = pallete.FG00, style = bold },
        Special = { fg = pallete.FG00, style = bold },
        Type = { fg = pallete.PR07, style = bold },
    }

    return syntax
end

-- Tema do editor
theme.loadEditor = function()

    local editor =
    {
        -- Numeração de linhas
        LineNr = { fg = pallete.FG00, style = bold },
        LineNrAbove = { fg = pallete.BG05, style = bold },
        LineNrBelow = { fg = pallete.BG05, style = bold },
        EndOfBuffer = { fg = pallete.BG00 },
        NonText = { fg = pallete.BG00 },

        -- Dobras
        Comment = { fg = pallete.BG05, style = bold },
        Folded = { link = "Comment" },

        -- Texto normal normal, em janela flutuante e inativa
        Normal = { fg = pallete.FG00, bg = pallete.BG00 },
        NormalFloat = { link = "Normal" },
        NormalNC = { link = "Normal" },

        -- Menu pop-up
        Pmenu = { fg = pallete.FG00, bg = pallete.BG00 },
        PmenuSel = { fg = pallete.PR05, style = bold },
        PmenuSbar = { },
        PmenuThumb = { bg = pallete.BG04 },

        -- Comentários de 'a fazer'
        Todo = { fg = pallete.FG01, style = bold },

        -- Barra de status
        StatusLine = { bg = pallete.BG01, fg=pallete.FG01, style = bold },
        StatusLineNC = { fg = pallete.BG02, style = bold },
        VertSplit = {},

        -- Barra de abas
        TabLineFill = { bg = pallete.BG01 },
        TabLine = { fg = pallete.BG05, style = bold },
        TabLineSel = { style = bold },
        Title = { fg = pallete.BG06, style = bold },

        -- Seleção
        Visual = { bg = pallete.BG04 },
        MatchParen = { fg = pallete.PR05, style = bold },
        Search = { bg = pallete.FG04, fg = pallete.FG00, style = bold .. nocombine },     
        IncSearch = { bg = pallete.FG04, fg = pallete.FG00, style = bold .. nocombine },
        CurSearch = { bg = pallete.PR01, fg = pallete.FG00, style = bold .. nocombine },
        Substitute = { fg = pallete.FG01 },
    

        -- Netrw
        netrwComment = { fg = pallete.FG00, style = bold },
        netrwList = { fg = pallete.PR00, style = bold },
        Directory = { style = bold }, 
        Question = { fg = pallete.BG06, style = bold },
        CursorLine = { fg = pallete.PR05, style = bold },

        -- Aparece quando g:cursorline = true.
        -- TODO: Ver isso aqui
        CursorLineNr = { fg = pallete.PR05, style = bold },


        -- Marcadores de identação [plugin]
        IndentBlanklineChar = { fg = pallete.BG04, style = bold },
    }

    return editor
end

theme.loadTex = function()

    local tex =
    {
        texRefArg = { style = bold },
        -- texMathSymbol = { fg = pallete.PR00, style = bold },
        -- texMathCmd = { fg = pallete.PR00, style = bold },
        texMathDelim = { fg = pallete.FG01, style = bold },
        texDelim = { fg = pallete.FG01, style = bold },
        texMathSuperSub = { fg = pallete.FG01, style = bold },

    }

    return tex
end


return theme
