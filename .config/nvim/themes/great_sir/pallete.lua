local pallete = 
{
    -- Fundo e variações
    BG00 = "#232335",
    BG01 = "#2A2A40",
    BG02 = "#34344D",
    BG03 = "#43435C",
    BG04 = "#56566C",
    BG05 = "#7F7F99",
    BG06 = "#ADADCC",
    BG07 = "#DADAF2", 

    -- BG00 = "#242435",
    -- BG01 = "#2A2A40", 
    -- BG01_2 = "#3E3E59", 
    -- BG02 = "#32324D", 
    -- BG02_2 = "#505073",
    -- BG03 = "#737399", 
    -- BG04 = "#8F8FB3", 
    -- BG05 = "#ADADCC",
    -- BG06 = "#CFCFE6", 
    -- BG07 = "#DADAF2", 

    -- Rosas
    PR00 = "#F2C2D4",
    PR01 = "#F090B6",
    PR02 = "#903963",
    PR03 = "#AC4175",
    PR04 = "#D45783",
    PR05 = "#ED75A6",
    PR06 = "#EDAFC8",
    PR07 = "#EDDDE3",

    -- PR00 = "#331A33",
    -- PR01 = "#804069",
    -- PR02 = "#903963",
    -- PR03 = "#AC4175",
    -- PR04 = "#D45783",
    -- PR05 = "#ED75A6",
    -- PR06 = "#EDAFC8",
    -- PR07 = "#EDDDE3",
    
    -- Azuis
    SC00 = "#36364D",
    SC01 = "#3E3E59",
    SC02 = "#4D4D66",
    SC03 = "#565673",
    SC04 = "#70708C",
    SC05 = "#8D8DA6",
    SC06 = "#ACACBF",
    SC07 = "#CECED9",
    
    -- Frente e variações
    FG00 = "#F5F0F2",
    FG01 = "#E0DFDC",
    FG02 = "#BFBEBB",
    FG03 = "#B3B1AF",
    FG04 = "#8C8B89",
    FG05 = "#595957",
    FG06 = "#F5F3EF",
    FG07 = "#F5F3EF",

    -- FG00 = "#F5F3EF",
    -- FG01 = "#E0DFDC",
    -- FG02 = "#BFBEBB",
    -- FG03 = "#B3B1AF",
    -- FG04 = "#8C8B89",
    -- FG05 = "#595957",
    -- FG06 = "#F5F3EF",
    -- FG07 = "#F5F3EF",
}

return pallete
