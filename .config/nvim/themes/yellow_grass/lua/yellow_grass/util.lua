local util = {}
local theme = require("yellow_grass.theme")

-- Percorre a tabela de cores para os grupos de sintaxe e
-- atribui as cores e estilos. Parâmetros não especificados
-- recebem o valor NONE.
util.highlight = function(group, color)

    local style = color.style and "gui = " .. color.style or "gui = NONE"
    local fg = color.fg and "guifg = " .. color.fg or "guifg = NONE"
    local bg = color.bg and "guibg = " .. color.bg or "guibg = NONE"
    local sp = color.sp and "guisp = " .. color.sp or "guisp = NONE"

    local hl = "highlight " .. group .. " " .. style .. " " .. fg .. " " .. bg .. " " .. sp

    vim.cmd(hl)
    if color.link then
        vim.cmd("highlight! link " .. group .. " " .. color.link)
    end
end

-- Percorre o dicionário colorSet e destaca cada grupo do dicionário
-- segundo os valores da tabela usando highlight.
function util.loadColorSet(colorSet)
	for group, colors in pairs(colorSet) do
		util.highlight(group, colors)
	end
end


-- Carrega o tema
function util.load()

	-- Define o ambiente do tema
	vim.cmd("hi clear")
	if vim.fn.exists("syntax_on") then
		vim.cmd("syntax reset")
	end

	vim.o.background = "dark"
	vim.o.termguicolors = true
	vim.g.colors_name = "yellow_grass"


	-- Carrega os destacamentos do editor
	local editor = theme.loadEditor()
	util.loadColorSet(editor)

	-- Carrega os destacamentos de sintaxe
	local syntax = theme.loadSyntax()
	util.loadColorSet(syntax)


    -- Destacamentos específicos para tipos de arquivos
    --
    -- Carrega destacamentos específicos para arquivos .tex.
    -- Incluindo destacamentos do vimtex
    vim.api.nvim_create_augroup("YellowGrassTex", { clear = true })
    vim.api.nvim_create_autocmd("FileType", {
        group = "YellowGrassTex",
        pattern = "tex",
        callback = function()
            local tex = require("yellow_grass.filetype.tex").loadFt()
            util.loadColorSet(tex)
        end,
    })

    -- Carrega os destacamentos específicos para arquivos .rs.
    -- Incluindo destacamentos do Treesitter
    vim.api.nvim_create_augroup("YellowGrassRust", { clear = true })
    vim.api.nvim_create_autocmd("FileType", {
        group = "YellowGrassRust",
        pattern = "rust",
        callback = function()
            local rust = require("yellow_grass.filetype.rust").loadFt()
            util.loadColorSet(rust)
        end,
    })

    -- Carrega os destacamentos específicos para arquivos .html.
    -- Incluindo destacamentos do Treesitter
    vim.api.nvim_create_augroup("YellowGrassHtml", { clear = true })
    vim.api.nvim_create_autocmd("FileType", {
        group = "YellowGrassHtml",
        pattern = "html",
        callback = function()
            local html = require("yellow_grass.filetype.html").loadFt()
            util.loadColorSet(html)
        end,
    })

    -- Carrega os destacamentos específicos para arquivos .md
    -- Incluindo destacamentos do Treesitter
    vim.api.nvim_create_augroup("YellowGrassMarkdown", { clear = true })
    vim.api.nvim_create_autocmd("FileType", {
        group = "YellowGrassMarkdown",
        pattern = "markdown",
        callback = function()

            -- Sintaxe
            local markdown = require("yellow_grass.filetype.markdown").loadFt()
            util.loadColorSet(markdown)

            -- Configurações
            local plugin = require("yellow_grass.ftplugin.markdown")
            plugin.load()

        end,
    })


    -- Arquivos .note
    -- Detecção do tipo de arquivo.
    local note_ft = require("yellow_grass.ftdetect.note")
    note_ft.detect()

    vim.api.nvim_create_augroup("YellowGrassNote", { clear = true })
    vim.api.nvim_create_autocmd("FileType", {
        group = "YellowGrassNote",
        pattern = "note",
        callback = function()

            -- Destacamento de sintaxe
            local note = require("yellow_grass.filetype.note").loadFt()
            util.loadColorSet(note)

            -- Configurações
            local plugin = require("yellow_grass.ftplugin.note")
            plugin.load()

            -- Sintaxe
            -- local syntax = require("yellow_grass.syntax.note")
            -- syntax.load_syntax()

        end,
    })
end

return util
