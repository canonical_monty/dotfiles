-- Sobrescreve alguns destacamentos do tema para arquivos .tex
-- além de configurar os destacamentos do vimtex.


local theme = {}
local pallete = require('yellow_grass.pallete')

-- Estilos
local bold = "bold"
local italic = "italic"
local nocombine = "nocombine"


theme.loadFt = function()

    local tex =
    {
        Constant = { fg = pallete.FG00, style = bold },

        -- Vimtex
        texRefArg = { style = bold },
        -- texMathSymbol = { fg = pallete.PR00, style = bold },
        -- texMathCmd = { fg = pallete.PR00, style = bold },
        texMathDelim = { fg = pallete.FG00, style = bold },
        texDelim = { fg = pallete.FG00, style = bold },
        texMathSuperSub = { fg = pallete.FG01, style = bold },
    }

    return tex
end

return theme
