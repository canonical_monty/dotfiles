-- Sobrescreve alguns destacamentos para arquivos .html além
-- de configurar os destacamentos do treesitter para Rust.


local theme = {}
local pallete = require('yellow_grass.pallete')

-- Estilos
local bold = "bold"
local italic = "italic"
local nocombine = "nocombine"


theme.loadFt = function()

    local html =
    {
        ["@tag"] = { fg = pallete.TT03, style = bold },
        ["@tag.delimiter"] = { fg = pallete.FG00, style = bold },
        ["@tag.attribute"] = { fg = pallete.PR02, style = bold },
        ["@string"] = { fg = pallete.FG00, style = bold },
    }

    return html
end

return theme
