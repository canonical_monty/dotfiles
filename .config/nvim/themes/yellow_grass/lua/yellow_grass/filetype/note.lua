-- Sobrescreve alguns destacamentos para arquivos .rs além
-- de configurar os destacamentos do treesitter para Rust.


local theme = {}
local pallete = require('yellow_grass.pallete')

-- Estilos
local bold = "bold"
local italic = "italic"
local nocombine = "nocombine"


theme.loadFt = function()

    local note =
    {
        -- Tem que usar essa notação ["..."] porque @function.macro
        -- e @variable aparentemente não são identificadores válidos.
        -- Isso é equivalente ao método de acesso Dic["chave"].
        HeaderDelim = { fg = pallete.PR01, style = bold },
        Header = { fg = pallete.PR02, style = bold },
        SubHeaderDelim = { fg = pallete.PR03, style = bold },
        SubHeader = { fg = pallete.PR03, style = bold },
        Pointer = { fg = pallete.SC04, style = bold },

        QuoteMark = { fg = pallete.TT02, style = bold },
        QuoteText = { fg = pallete.FG01 },
    }

    return note
end

return theme
