-- Sobrescreve alguns destacamentos para arquivos .rs além
-- de configurar os destacamentos do treesitter para Rust.


local theme = {}
local pallete = require('yellow_grass.pallete')

-- Estilos
local bold = "bold"
local italic = "italic"
local nocombine = "nocombine"


theme.loadFt = function()

    local rust =
    {
        -- Tem que usar essa notação ["..."] porque @function.macro
        -- e @variable aparentemente não são identificadores válidos.
        -- Isso é equivalente ao método de acesso Dic["chave"].
        ["@function.macro"] = { fg = pallete.FG00, style = bold },
        ["@variable"] = { fg = pallete.FG00 },
        ["@string.escape"] = { link = "@string" },
        ["@comment.documentation"] = { fg = pallete.FG03, style = bold },
    }

    return rust
end

return theme
