-- Sobrescreve alguns destacamentos para arquivos .rs além
-- de configurar os destacamentos do treesitter para Rust.


local theme = {}
local pallete = require('yellow_grass.pallete')

-- Estilos
local bold = "bold"
local italic = "italic"
local nocombine = "nocombine"


theme.loadFt = function()

    local markdown =
    {
        -- Tem que usar essa notação ["..."] porque @function.macro
        -- e @variable aparentemente não são identificadores válidos.
        -- Isso é equivalente ao método de acesso Dic["chave"].
        ["@text.emphasis"] = { fg = pallete.TT04, style = italic },
        ["@text.strong"] = { fg = pallete.PR02, style = bold},
        ["@text.title.1"] = { fg = pallete.PR02, style = bold},
        ["@punctuation.special"] = { fg = pallete.SC04, style = bold},
    }

    return markdown
end

return theme
