local theme = {}
local pallete = require('yellow_grass.pallete')

-- Estilos
local bold = "bold"
local italic = "italic"
local nocombine = "nocombine"


-- Tema dos grupos básicos
theme.loadSyntax = function()

    local syntax =
    {
        Statement = { fg = pallete.PR02, style = bold },
        Identifier = { fg = pallete.SC04 },
        PreProc = { style = bold },
        Constant = { fg = pallete.PR04, style = bold },
        Special = { fg = pallete.FG00, style = bold },
        Type = { fg = pallete.TT04, style = bold },
    }

    return syntax
end

-- Tema do editor
theme.loadEditor = function()

    local editor =
    {
        -- Numeração de linhas
        LineNr = { fg = pallete.FG00, style = bold },
        LineNrAbove = { fg = pallete.BG04, style = bold },
        LineNrBelow = { fg = pallete.BG04, style = bold },
        EndOfBuffer = { fg = pallete.BG00 },
        NonText = { fg = pallete.BG00 },

        -- Dobras
        Comment = { fg = pallete.BG05, style = bold },
        Folded = { link = "Comment" },

        -- Texto normal normal, em janela flutuante e inativa
        Normal = { fg = pallete.FG00, bg = pallete.BG01 },
        NormalFloat = { link = "Normal" },
        NormalNC = { link = "Normal" },

        -- Correção ortográfica
        SpellBad = { bg = pallete.TT03, style = bold },
        SpellRare = { bg = pallete.SC03, style = bold },

        -- Menu pop-up
        Pmenu = { fg = pallete.FG00, bg = pallete.BG01 },
        PmenuSel = { fg = pallete.TT03, style = bold },
        PmenuSbar = { },
        PmenuThumb = { bg = pallete.BG04 },

        -- Comentários de 'a fazer'
        Todo = { fg = pallete.TT03, style = bold },

        -- Barra de status
        StatusLine = { bg = pallete.BG02, fg=pallete.FG01, style = bold },
        StatusLineNC = { fg = pallete.BG02, style = bold },
        VertSplit = {},

        -- Barra de abas
        TabLineFill = { bg = pallete.BG02 },
        TabLine = { fg = pallete.BG05, style = bold },
        TabLineSel = { style = bold },
        Title = { fg = pallete.BG06, style = bold },

        -- Seleção
        MatchParen = { fg = pallete.TT03, style = bold },
        Visual = { bg = pallete.BG03 },
        Search = { bg = pallete.FG04, fg = pallete.FG00, style = bold .. nocombine },     
        IncSearch = { bg = pallete.SC04, fg = pallete.FG00, style = bold .. nocombine },
        CurSearch = { bg = pallete.PR01, fg = pallete.FG00, style = bold .. nocombine },
        Substitute = { bg = pallete.TT03, fg = pallete.FG00 },
    

        -- Netrw
        netrwComment = { fg = pallete.FG00, style = bold },
        netrwList = { fg = pallete.PR00, style = bold },
        Directory = { style = bold }, 
        Question = { fg = pallete.BG06, style = bold },
        CursorLine = { fg = pallete.TT03, style = bold },

        -- Aparece quando g:cursorline = true.
        -- TODO: Ver isso aqui
        CursorLineNr = { fg = pallete.PR05, style = bold },


        -- Marcadores de identação [plugin]
        IblIndent = { fg = pallete.BG02, style = bold },
        IblScope = { fg = pallete.PR01, style = bold },


        -- Diff
        DiffAdd = { bg = pallete.SC04 },
        DiffChange = { bg = pallete.PR02 },
        DiffDelete = { bg = pallete.TT03 },
        DiffText = { bg = pallete.PR04, fg = pallete.BG00 },
        SignColumn = { bg = pallete.BG00, fg = pallete.PR01 },
        FoldColumn = { bg = pallete.BG00, fg = pallete.PR01 },

    }

    return editor
end

return theme
