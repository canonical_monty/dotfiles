local filetype = {}

function filetype.detect()

    vim.api.nvim_create_augroup("YellowGrassDetect", { clear = false })
    vim.api.nvim_create_autocmd( { "BufRead", "BufNewFile", "StdinReadPost" }, {
        group = "YellowGrassDetect",
        pattern = "*.note",
        callback = function()
            vim.cmd("set filetype=note")
        end
    })
end

return filetype
