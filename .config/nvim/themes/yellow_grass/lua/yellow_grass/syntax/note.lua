local syntax = {}

syntax.regions = function(name, params)

    local matchgroup = params.matchgroup and "matchgroup = " .. params.matchgroup or ""

    local i_match = "start=" .. params.i_match
    local f_match = "end=" .. params.f_match 
    local contains = params.contains and "contains = " .. params.contains or ""

    local region_delim = i_match .. " " .. f_match .. " " .. contains
    local syn = "syntax region " .. name .. " " .. matchgroup .. " " .. region_delim

    vim.cmd(syn)

end

syntax.load_syntax = function()

    local syntax_regions = 
    {
        H1 = 
        { 
            matchgroup = "HeaderDelim",
            i_match = [["^\s*:: "]], 
            f_match = [["$"]],
        },

        H2 = 
        { 
            matchgroup = "HeaderDelim", 
            i_match = [["^\s*::: "]],
            f_match = [["$"]],
        },

        H3 = 
        {
            matchgroup = "SubHeaderDelim",
            i_match = [["^\s*:::: "]],
            f_match = [["\s\{4\}"]],
        },

        H4 = 
        { 
            matchgroup = "SubHeaderDelim",
            i_match = [["\s*::::: "]],
            f_match = [["\s\{4\}"]],
        },
    }

    -- if not vim.fn.exists("b:current_syntax") then

    -- Regiões
    for region, params in pairs(syntax_regions) do
        syntax.regions(region, params)
    end

    vim.cmd("hi def link H1 Header")
    vim.cmd("hi def link H2 Header")
    vim.cmd("hi def link H3 SubHeader")
    vim.cmd("hi def link H4 SubHeader")

    -- Provisório
    vim.cmd("syn match Pointer \">>\\+\"")

    local quote_mark = [["||\%(\s\|$\)"]]
    vim.cmd("syn match Quotesyntax.rk " .. quote_mark .. " nextgroup=QuoteText")
    vim.cmd("syn match QUoteText " .. [[".\{-}$"]] .. " contained")

    vim.cmd("syntax enable")
    vim.b.current_syntax="note"

end

return syntax
