local pallete = 
{
    -- Fundo
    BG00 = "#09080F",
    BG01 = "#14131A",
    BG02 = "#292730",
    BG03 = "#3C3A42",
    BG04 = "#535259",
    BG05 = "#6D6C73",

    -- Frente 
    FG00 = "#F5F0F2",
    FG01 = "#E0DFDC",
    FG02 = "#BFBEBB",
    FG03 = "#B3B1AF",
    FG04 = "#8C8B89",
    FG05 = "#595957",
    FG06 = "#F5F3EF",
    FG07 = "#F5F3EF",


    -- Amarelo --- Destaque primário
    PR00 = "#C17D01",
    PR01 = "#F7AB00",
    PR02 = "#FFC95E",
    PR03 = "#FFD596",
    PR04 = "#FFE0BF",

    -- Tom de amarelo claro demais
    -- PR02 = "#FED35E",
    -- PR03 = "#FFF596",
    -- PR04 = "#FFFFB3",


    -- Verde --- Destaque secundário
    SC00 = "#3E4A3E",
    SC01 = "#315246",
    SC02 = "#23594F",
    SC03 = "#369282",
    SC04 = "#49CAB4",


    -- Rosa --- Destaque terciário
    TT00 = "#BC5EA3",
    TT01 = "#CF67B2",
    TT02 = "#E35A9C",
    TT03 = "#F74C86",
    TT04 = "#F85C91",
}

return pallete
