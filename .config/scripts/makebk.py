#!/usr/bin/env python
## Créditos: https://github.com/SiddharthPant/booky
import sys
import os

## Constantes
STARTCHAR = "{"
ENDCHAR = "}"

## Converte para metadados de djvu
def generate_indent(level):

    indent = ""
    for _ in range(level):
        indent = indent + "    "

    return indent

def line_to_djvu_format(line, level):

    commaIndex = line.rfind(',')
    title = line[:commaIndex]
    pageNo = line[commaIndex + 1:].strip()
    indent = generate_indent(level)

    print("{}(\"{}\"".format(indent, title.strip()))
    print("{}\"#{}\"\t".format(indent, pageNo.strip()), end="")

def to_djvu_format(lines):

    level = 0
    lenght = len(lines)

    print("(bookmarks")
    for i in range(lenght):
        line = lines[i].strip()

        if line == STARTCHAR:
            level = level + 1
        elif line == ENDCHAR:
            level = level - 1
            print("{})".format(generate_indent(level)))
        else:
            line_to_djvu_format(line, level)
            
            if i + 1 < lenght:
                next_line = lines[i + 1].strip()
                if next_line == STARTCHAR:
                    print("")
                elif next_line == ENDCHAR:
                    print(")")
                else:
                    print(")")



## Converte para metadados de pdf
def to_pdf_format(lines):

    level = 0
    for line in lines:
        line = line.strip()
        if line == STARTCHAR:
            level = level + 1
        elif line == ENDCHAR:
            level = level - 1
        else:
            commaIndex = line.rfind(',')
            title = line[:commaIndex]
            pageNo = line[commaIndex + 1:].strip()
            print("BookmarkBegin")
            print("BookmarkTitle:", title.strip())
            print("BookmarkLevel:", level)
            print("BookmarkPageNumber:", pageNo.strip())



## Descola as páginas marcadas com `>`:
import re

def offset_pages(filename, offset):

    file = open(filename, "r")
    page_regex = re.compile(r",[ ]*>([0-9]*)$")
    
    ## Descola as páginas.
    new_lines = []
    for line in file.readlines():
        match = page_regex.search(line)

        if match != None:
            page_no = match.group(1)
            new_page_no = ", " + str(int(page_no) + offset)
            new_lines.append(page_regex.sub(new_page_no, line))
        else:
            new_lines.append(line)
        

    file.close()

    ## Sobrescreve o arquivo original
    file = open(filename, "w")
    for new_line in new_lines:
        file.write(new_line)
        file.truncate()

    file.close()



### Localiza as coisas para português
__TRANSLATIONS = {
    "show this help message and exit": "Mostra esta mensagem de ajuda e finaliza.",
    "options": "Opções",
    "usage": "Uso",
    "positional arguments": "Argumentos posicionais",
}

def convertArgparseMessages(text):

    if text in __TRANSLATIONS:
        text = __TRANSLATIONS[text]

    return text

import gettext
gettext.gettext = convertArgparseMessages

# Aparentemente o operador or não funciona nesse caso porque tentar acessar um valor não
# contido no dicionário retorna um erro ao invés de False
# gettext.gettext = lambda text: __TRANSLATIONS[text] or text



### Programa
import argparse

def main():

    parser = argparse.ArgumentParser()

    pdf_help = "Converte file para o formato adequado de marcações para arquivos .pdf."
    parser.add_argument("-p", "--pdf", help=pdf_help, action="store_true")
    djvu_help = "Converte file para o formato adequado de marcações para arquivos .djvu."
    parser.add_argument("-d", "--djvu", help=djvu_help, action="store_true")
    offset_help = "Desloca as páginas marcadas por '>' em 'offset' unidades"
    parser.add_argument("-o", "--offset", help=offset_help, type=int) 
    parser.add_argument("file", help="Arquivo a ser convertido.")

    args = parser.parse_args()
    if args.pdf and args.djvu:
        print("Erro! Apenas um dos argumentos -p/--pdf, -d/--djvu deve ser passado.")
    else:
        ## Descola as páginas
        if args.offset:
            offset_pages(args.file, args.offset)

        ## Faz a mágica
        file = open(args.file, "r")
        if args.pdf:
            to_pdf_format(file.readlines())
        elif args.djvu:
            to_djvu_format(file.readlines())
        else:
            print("Erro! Um dos argumentos -p/--pdf, -d/--djvu é necessário.")


main()





### Versão que produz um arquivo. Achei melhor deixar o print para 
### usar redireção, no entanto.
### def to_pdf_format(filename):
### 
###     file = open(filename, "r")
###     basename = os.path.basename(filename)
###     new_file = basename.replace(".bk.info", ".bk.data")
###     new_file = open(new_file, "w")
### 
###     level = 0
###     for line in file.readlines():
###         line = line.strip()
###         if line == STARTCHAR:
###             level = level + 1
###         elif line == ENDCHAR:
###             level = level - 1
###         else:
###             commaIndex = line.rfind(',')
###             title = line[:commaIndex]
###             pageNo = line[commaIndex + 1:].strip()
###             new_file.write("BookmarkBegin\n")
###             new_file.write("BookmarkTitle: {}\n".format(title.strip()))
###             new_file.write("BookmarkLevel: {}\n".format(level))
###             new_file.write("BookmarkPageNumber: {}\n".format(pageNo.strip()))
