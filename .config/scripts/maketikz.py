#!/usr/bin/env python
## Créditos https://github.com/gillescastel/inkscape-figures
import sys
import os
import re
from pathlib import Path


### Localiza as coisas para português
__TRANSLATIONS = {
    "show this help message and exit": "Mostra esta mensagem de ajuda e finaliza.",
    "options": "Opções",
    "usage": "Uso",
    "positional arguments": "Argumentos posicionais",
    "%(prog)s: error: %(message)s\n": "%(prog)s: erro: %(message)s\n"
}

def convertArgparseMessages(text):

    if text in __TRANSLATIONS:
        text = __TRANSLATIONS[text]

    return text

import gettext
gettext.gettext = convertArgparseMessages

# Aparentemente o operador or não funciona nesse caso porque tentar acessar um valor não
# contido no dicionário retorna um erro ao invés de False
# gettext.gettext = lambda text: __TRANSLATIONS[text] or text



### Programa
import argparse

def main():

    parser = argparse.ArgumentParser()
    sub_parser = parser.add_subparsers(help="Subcomandos", dest='subcommand')

    prep_parser = sub_parser.add_parser("prep", help="Prepara o nome da figura.")
    prep_parser.add_argument('name', help="Nome da figura a ser criada.")

    run_parser = sub_parser.add_parser("run", help="Cria a figura.")
    run_parser.add_argument('name', help="Nome não preparado da figura a ser criada.")
    run_parser.add_argument('path', help="Caminho completo de onde a figura será criada.")

    open_parser = sub_parser.add_parser('open', help="Abre uma figura.")
    open_parser.add_argument('line', help="Caminho da figura a ser aberta.")

    args = parser.parse_args()
    if args.subcommand == 'prep':
        print(prep_name(args.name))
    elif args.subcommand == 'run':

        args = parser.parse_args()

        ## Se o caminho 'path' não existir, o cria.
        path = Path(args.path).absolute()
        if not path.exists():
            path.mkdir()
     
        ## Título e o nome da figura
        title = args.name
        ## Isso daqui possivelmente pode ser melhorado para eliminar coisas como 'de' e afins
        figure_name = prep_name(args.name)
        figure_path = path / figure_name
     
        ## Lida com a figura
        if figure_path.exists():
            ## Decidir o que fazer nesse caso. Por hora, não faz nada
            pass
        else:
            figure = open(figure_path, 'w')
            figure.write(r"\begin{tikzpicture}")
            figure.write("\n\n\n")
            figure.write(r"\end{tikzpicture}")
     
     
        ## Retorna o código a ser substituído no .tex
        print(latex_template(figure_path.stem, title))

    elif args.subcommand == 'open':

        args = parser.parse_args()

        match = re.search(r"\\incfig\{(.*)\}", args.line);
        if match == None:
            ## print("Figura inexistente")
            ## Tem que retornar o código de erro para o vim verificar
            sys.exit(9)
        else:
            figure_name = match.group(1) + ".tikz"
            print(figure_name)



## Trata o nome passado para o comando e produz o nome final do arquivo .tikz
def prep_name(name):
    
    ## Adicionar regex para pegar o nível de indentação
    return name.strip().replace(' ', '-').lower() + '.tikz'


## Código a ser substituido no arquivo .tex
def latex_template(name, title):

    return '\n'.join((
        rf"\incfig{{{name}}}",
        rf"\caption{{}}",
        rf"\label{{fig:{name}}}",
    ))
                     

## ## Código a ser substituido no arquivo .tex
## def latex_template(name, title):
## 
##     return '\n'.join((
##         r"\begin{center}",
##         rf"    \incfig{{{name}}}",
##         rf"    \captionof{{figure}}{{{title}}}",
##         rf"    \label{{fig:{name}}}",
##         r"\end{center}"
##     ))



if __name__ == "__main__":
    main()

