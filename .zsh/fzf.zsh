### FZF

## Ativa os comandos do FZF
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

## Configurações
local FZF_BASE_CFG="--extended --cycle --multi --no-mouse"
local FZF_LAYOUT="--height=50% --layout=reverse --border=sharp --margin 1% --info=hidden"
local FZF_DISPLAY="--ansi --tabstop=4 --prompt=':: ' --pointer='>' --marker=▌ --border=top"
local FZF_COLORS="--color='border:16,hl:13,hl+:13,prompt:02,bg+:17'
                  --color='fg+:16,pointer:02,marker:02'
                  --color='header:01:bold'"

export FZF_DEFAULT_OPTS="$FZF_BASE_CFG $FZF_LAYOUT $FZF_DISPLAY $FZF_COLORS"
export FZF_ALT_C_COMMAND="fd --exclude '.git' --type d . $HOME"

## ## Usa FD para gerar a entrada do FZF
## export FZF_DEFAULT_COMMAND="fd --hidden --exclude '.git' . $HOME"
## export FZF_CTRL_T_COMMAND="fd --hidden --exclude '.git' --type f"
## export FZF_ALT_C_COMMAND="fd --exclude '.git' --type d . $HOME"
## 
## _fzf_compgen_path() {
##     fd --hiden --follow --exclude ".git" . "$1"
## }
## 
## _fzf_compgen_dir() {
##     fd --type d --hidden --follow --exclude ".git" . "$1"
## }

