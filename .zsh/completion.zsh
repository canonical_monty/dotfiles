### Este arquivo define as opções da compleção do zsh.
### Para detalhes, ver man zshcompsys seção STANDARD STYLES
### Existem >muitas< opções para se configurar.

## Define as funções de compleção. Ver man zshcompsys seçao CONTROL FUNCTIONS
zstyle ':completion:*' completer _extensions _complete _approximate

## Define o uso de cache para acelerar (possivelmente) a compleção.
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$ZDOTDIR/.zcompcache"

## Define o uso de um menu de compleção. 
## Permite navegar livremente pelas opções ao invés de ir em sequência
## com tab como é usual. Ver complist?
zstyle ':completion:*' menu select ##search

## Mensagens
## Formata as mensagens exibidas pelo completador. Por exemplo, nomes de grupos. 
zstyle ':completion:*:*:*:*:descriptions' format '%B%F{02}:: %d %f%b'

## Grupos
## Separa os resultados em grupos. Por exemplo separa aliases de comandos.
zstyle ':completion:*' group-name ''

## Critérios de busca
## Configura padrões de busca: primeiro a busca normal, depois busca ignorando
## maíusculas e minúsculas e depois busca interpretando o contexto como uma
## substring. Por exemplo: Piece em One Piece.
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=* r:|=*' 'l:|=*r:|=*'

## zstyle ':completion:*' matcher-list '' 
#	'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 
#	'+l:|=* r:|=*'

## Para detalhes ver man zshcompwid 'COMPLETION MATCHING CONTROL'

## Cores
## Colore os resultados de compleção com cores do ls
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}




