## Configurações do prompt


### Estados do git
local MODIFIED="!"
local ADDED="+"
local DELETED="-"
local RENAMED=">>"
local UNTRACKED="?"
## local UNMERGED=""
## local STASHED=""
## local BEHIND=""
## local AHEAD=""

local git_prompt_modified="%{%B%F{#13} $MODIFIED%f%b%}"
local git_prompt_added="%{%B%F{16} $ADDED%f%b%}"
local git_prompt_deleted="%{%B%F{16} $DELETED%f%b%}"
local git_prompt_renamed="%{%B%F{16} $RENAMED%f%b%}"
local git_prompt_untracked="%{%B%F{16} $UNTRACKED%f%b%}"
## local git_prompt_unmerged="%{%B%F{#FF980D} $UNMERGED%f%b%}"
## local git_prompt_stashed="%{%B%F{#FF980D} $STASHED%f%b%}"
## local git_prompt_behind="%{%B%F{#FF980D} $BEHIND%f%b%}"
## local git_prompt_ahead="%{%B%F{#FF980D} $AHEAD%f%b%}"


## Cria a string com as informações do git
git_prompt () {
    if [[ $(git rev-parse --is-inside-work-tree 2>/dev/null) == "true" ]] then

        ## Nome do ramo atual
        branch_name=$(git branch | grep '*' | sed 's/* //g')
        git_prompt="%{%B%F{13} -> $branch_name%f%b%}"
        

        git_status=$(echo -e -n `git status --porcelain`)
        added=$(echo $git_status | sed 's,.*\(A \).*,\1,g')
        modified=$(echo $git_status | sed 's,.*\(M \).*,\1,g')
        deleted=$(echo $git_status | sed 's,.*\(D \).*,\1,g')
        renamed=$(echo $git_status | sed 's,.*\(R \).*,\1,g')
        untracked=$(echo $git_status | sed 's,.*\(?? \).*,\1,g')

        ## print -P $deleted
        ## print -P ${#deleted}
        ## print -P $modified
        ## print -P ${#modified}
        
        if [[ $modified == "M " ]] then
            git_prompt+=$git_prompt_modified
        fi

        if [[ $added == "A " ]] then
            git_prompt+=$git_prompt_added
        fi

        if [[ $deleted == "D " ]] then
            git_prompt+=$git_prompt_deleted
        fi

        if [[ $renamed == "R " ]] then
            git_prompt+=$git_prompt_renamed
        fi

        if [[ $untracked == "?? " ]] then
            git_prompt+=$git_prompt_untracked
        fi

        echo -n "$git_prompt"
    else
        echo -n ""
    fi
}

## Imprime o prompt direito
right_prompt() {
    echo -n "%~$(git_prompt)"
}


## Formatação do nome da máquina (host)
host_prompt() {
	echo -n "%B%M%b"
}

## Formatação do nome do usuário
usr_prompt() {
	echo -n "%B%F{13}%n%f%b"
}

## Indicador do prompt 
prompt_arrow() {
    old="❯"
    arrow="☭"
 	echo -n "%B%F{01}$arrow%f%b "
}

## Imprime o prompt esquerdo
left_prompt() {
	echo -n "$(host_prompt):$(usr_prompt)"
}



### Cria a primeira linha do prompt
prompt_first_line() {
    left="$(left_prompt)"
    right="%{%B%~%b%}$(git_prompt)"

    ### Calcula o espaço removendo o código ANSI de formatação
    left_bare=$(print -P $left | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g")
    right_bare=$(print -P $right | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g")

    white_spaces=$(($COLUMNS-${#left_bare}-${#right_bare}))

    ### Imprime a primeira linha
    print -P $left${(l:white_spaces:: :)}$right
}


#### Prompt
## Permite a expansão de funções no prompt. Ver manual
setopt promptsubst

## Primeira linha do prompt
precmd() {
    prompt_first_line
}

## Indicador
PROMPT="$(prompt_arrow)"

