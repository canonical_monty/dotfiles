### Plugin: zsh-autosuggestions
source "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"

## Configurações
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=02,bold"


### Plugin: zsh-syntax-highlighting
source "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

## Configurações
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main)

typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[command]="fg=01,bold"
ZSH_HIGHLIGHT_STYLES[alias]="fg=01,bold"
ZSH_HIGHLIGHT_STYLES[builtin]="fg=01,bold"
ZSH_HIGHLIGHT_STYLES[precommand]="fg=01,bold"
ZSH_HIGHLIGHT_STYLES[path]="bold"
ZSH_HIGHLIGHT_STYLES[unknown-token]=""

## A configurar
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]=""
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]=""
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]=""
ZSH_HIGHLIGHT_STYLES[back-quoted-argument-unclosed]=""
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]=""
ZSH_HIGHLIGHT_STYLES[single-quoted-argument-unclosed]=""
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]=""
ZSH_HIGHLIGHT_STYLES[double-quoted-argument-unclosed]=""

