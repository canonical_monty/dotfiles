## Configurações do ZSH
source "$HOME/.zshenv"


### Cores
source "$ZDOTDIR/colors.zsh"

### Autocompleção
source "$ZDOTDIR/completion.zsh"
autoload -Uz compinit; compinit

### Prompt
source "$ZDOTDIR/prompt.zsh"


### Aliases

alias ls='ls --color -h -x --group-directories-first' # --sort=extension'
alias scrot='cd ~/IM-Imagens/SC-Screenshots && scrot'

## Alias para o repositório dos dotfiles
alias config='/usr/bin/git --git-dir=$DOTSDIR --work-tree=$HOME'

## Alias neovim + compilação
alias vtex='nvim +VimtexCompile'

### Paginas dos manuais
### Usa o bat para colorir as páginas dos manuais
#export MANPAGER="sh -c 'col -bx | bat -l man -p'"


### FZF
source "$HOME/.zsh/fzf.zsh"


### Plugins
source "$HOME/.zsh/plugins.zsh"
